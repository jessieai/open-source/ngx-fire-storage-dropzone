# Angular Firebase Storage Dropzone

<a href="https://badge.fury.io/js/ngx-fire-storage-dropzone"><img src="https://badge.fury.io/js/ngx-fire-storage-dropzone.svg" align="right" alt="npm version" height="18"></a>

This is an Angular dropzone library for Firebase Storage.

It is inspired from [Angular Dropzone Wrapper](https://github.com/zefoy/ngx-dropzone-wrapper).

### Quick links

[Example application](https://zefoy.github.io/ngx-fire-storage-dropzone/)
 | 
[Plunker template](http://plnkr.co/edit/lRFrzs7FJ9Q0TSd5tTko?p=preview)
 | 
[Dropzone documentation](http://www.dropzonejs.com/#configuration-options)

### Building the library

```bash
npm install
npm start
```

### Running the example

```bash
cd example
npm install
npm start
```

### Library development


```bash
npm link
cd example
npm link ngx-fire-storage-dropzone
```

### Installing and usage

```bash
npm install ngx-fire-storage-dropzone --save
```

##### Load the module for your app (with global configuration):

Providing the global configuration is optional and when used you should only provide the configuration in your root module.

```javascript
import { DropzoneModule } from 'ngx-fire-storage-dropzone';
import { DROPZONE_CONFIG } from 'ngx-fire-storage-dropzone';
import { DropzoneConfigInterface } from 'ngx-fire-storage-dropzone';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
 // Change this to your upload POST address:
  url: 'https://httpbin.org/post',
  maxFilesize: 50,
  acceptedFiles: 'image/*'
};

@NgModule({
  ...
  imports: [
    ...
    DropzoneModule
  ],
  providers: [
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    }
  ]
})
```

##### Use it in your HTML template (with custom configuration):

This library provides two ways to create a Dropzone element, component for simple use cases and directive for more custom use cases.

**COMPONENT USAGE**

Simply replace the element that would ordinarily be passed to `Dropzone` with the dropzone component.

**NOTE:** Component provides couple additional features from directive such as the placeholder image. If you don't need them or want to create custom component then you might want to use the directive instead.

```html
<dropzone [config]="config" [message]="'Click or drag images here to upload'" (error)="onUploadError($event)" (success)="onUploadSuccess($event)"></dropzone>
```

```javascript
[config]                // Custom config to override the global defaults.

[disabled]              // Disables / detaches Dropzone from the element.

[message]               // Message to show for the user on the upload area.
[placeholder]           // Placeholder image to be shown as the upload area.

[useDropzoneClass]      // Use 'dropzone' class (use provided default styles).

(error)                 // Event handler for the Dropzone upload error event.
(success)               // Event handler for the Dropzone upload success event.
(canceled)              // Event handler for the Dropzone upload canceled event.

(<dropzoneEvent>)       // All Dropzone events / callbacks work as bindings.
                        // Event names are in camel case (not lower case).
                        // Example: maxfilesreached -> maxFilesReached
```

**DIRECTIVE USAGE**

When using only the directive you need to provide your own theming or import the default theme:

```css
@import '~dropzone/dist/min/dropzone.min.css';
```

Dropzone directive can be used in form or div element with optional custom configuration:

```html
<div class="dropzone" [dropzone]="config" (error)="onUploadError($event)" (success)="onUploadSuccess($event)"></div>
```

```javascript
[dropzone]              // Can be used to provide optional custom config.

[disabled]              // Disables / detaches Dropzone from the element.

(error)                 // Event handler for the Dropzone upload error event.
(success)               // Event handler for the Dropzone upload success event.
(canceled)              // Event handler for the Dropzone upload canceled event.

(<dropzoneEvent>)       // All Dropzone events / callbacks work as bindings.
                        // Event names are in camel case (not lower case).
                        // Example: maxfilesreached -> maxFilesReached
```

##### Available configuration options (custom / global configuration):

This library supports all Dropzone configuration options and few extra options for easier usage.

**LIBRARY OPTIONS**

```javascript
autoReset               // Time for resetting component after upload (Default: null).
errorReset              // Time for resetting component after an error (Default: null).
cancelReset             // Time for resetting component after canceling (Default: null).
```

**DROPZONE OPTIONS**

```javascript
url                     // Upload url where to send the upload request (Default: '').
method                  // HTTP method to use communicating with the server (Default: 'post').
headers                 // Object of additional headers to send to the server (Default: null).
paramName               // Name of the file parameter that gets transferred (Default: 'file').
maxFilesize             // Maximum file size for the upload files in megabytes (Default: null).
acceptedFiles           // Comma separated list of mime types or file extensions (Default: null).
```

For more detailed documentation with all the supported Dropzone events / options see the Dropzone documentation.

##### Available control / helper functions (provided by the directive):

```javascript
dropzone()              // Returns reference to the Dropzone instance for full API access.

reset(cancel?)          // Removes all processed files (optionally cancels uploads as well).
```

Above functions can be accessed through the directive reference (available as directiveRef in the component).
