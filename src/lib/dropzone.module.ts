import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';

import { FireStorageDropzoneComponent } from './dropzone.component';
import { FireStorageDropzoneDirective } from './dropzone.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [FireStorageDropzoneComponent, FireStorageDropzoneDirective],
  exports: [
    CommonModule,
    FireStorageDropzoneComponent,
    FireStorageDropzoneDirective,
  ],
})
export class FireStorageDropzoneModule {}
